/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnimap.client;

import com.samrj.devil.game.Game;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.Font;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;

/**
 *
 * @author angle
 */
public class MainClient implements Client {
    
    public Screen screen;

    public static void main(String args[]) {
        Client client = new MainClient();
        client.run();
    }
    
    @Override
    public void preInit() {
        Game.setDebug(true);
        
        //OpenGL context should be forward-compatible, i.e. one where all
        //functionality deprecated in the requested version of OpenGL is
        //removed. In the core profile, immediate mode OpenGL is deprecated.
        Game.hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        Game.hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        
        //OpenGL 3.2 gives good access to most modern features, and is
        //supported by most hardware.
        Game.hint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        Game.hint(GLFW_CONTEXT_VERSION_MINOR, 2);
        
        Game.setFullscreen(false);
        Game.setVsync(true);
        Game.setTitle("Omnimap");
    }

    @Override
    public void init() {
        try {
            DUI.setFont(new Font(new FileInputStream("resources/Helvetica-Normal.ttf")));
        } catch (IOException ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        changeScreen(new TitleScreen(this));
    }
    
    @Override
    public void mouseMoved(float x, float y) {
        screen.mouseMoved(x, y);
    }

    @Override
    public void mouseButton(int button, int action, int mods) {
        screen.mouseButton(button, action, mods);
    }
    
    @Override
    public void key(int key, int action, int mods) {
        screen.key(key, action, mods);
    }
    
    @Override
    public void resize(int width, int height) {
        screen.resize(width, height);
    }
    
    @Override
    public void step(float dt) {
        screen.step(dt);
    }
    
    @Override
    public void render() {
        screen.render();
    }
    
    public void changeScreen(Screen newScreen) {
        if (screen != null) screen.destroy(false);
        screen = newScreen;
        screen.init();
    }

    @Override
    public void destroy(Boolean crashed) {
        screen.destroy(crashed);
        
        DUI.font().destroy();
        
        if (crashed) DGL.setDebugLeakTracking(false);

        DUI.destroy();
        DGL.destroy();
    }
    
}
